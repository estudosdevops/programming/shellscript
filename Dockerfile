FROM koalaman/shellcheck-alpine:stable

# Metadata
LABEL maintainer="Fabio Coelho <fabiocruzcoelho@gmail.com>"

ENV LC_ALL=en_US.UTF-8

COPY examples /examples

RUN apk add --update --no-cache \
    bash=~5.0 \
    # Removing package list.
    && rm -rf /var/cache/apk/* \
    && rm -rf /tmp/* \
    && echo "" > /root/.ash_history \
    # change default shell from ash to bash
    && sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd
