#!/usr/bin/env bash
#
# zuser
# Lista, adiciona e remove usuários do sistema Z
#
# Uso: zuser [ lista | adiciona | remove ]
#
# Requesitos: bantex.sh
#
#  06/05/2020 Fabio Coelho
#
set -e


# Se não passar nenhum argumento, mostra a menssagem de ajuda
[ "$1" ] || {
    echo
    echo "Uso: zuser [ lista | adiciona | remove ]"
    echo
    echo "  lista    - Lista todos os usuários do sistema"
    echo "  adiciona - Adiciona um usuário novo no sistema"
    echo "  lista    - Remove um usuário do sistema"
    echo
    exit 0
}

# Localização do arquivo do banco de dados
BANCO="usuarios.txt"
echo "$BANCO"

# Inclui o gerenciador do banco
source bantex-4.sh || {
    echo "Ops, ocorreu algum erro no gerenciador do banco"
    exit 1
}

# Lida com os comandos recebidos
case "$1" in

    lista)
        # Lista dos logins (apaga a primeira linha)
        pega_campo 1 | sed 1d
    ;;

    adiciona)
        echo -n "Digite o login do usuário novo: "
        read -r login

        # Temos algo?
        [ "$login" ] || {
            echo "O login não pode ser vazio, tente novamente."
            exit 1
        }

        # Primeiro confere se já não existe esse usuário
        tem_chave "$login" && {
            echo " O usuário '$login' já foi cadastrado."
            exit 1
        }

        # Ok, é um usuário novo, prosseguimos
        echo -n "Digite o nome completo: "
        read -r nome
        echo -n "Digite a idade: "
        read -r idade
        echo -n "È do sexto masculino ou feminino? [MF] "
        read -r sexo
        echo

        # Dados obtidos, hora de mascarar eventuais dois-pontos
        nome=$(echo "$nome" | mascara)

        # Tudo pronto, basta inserir
        insere_registro "$login:$nome:$idade:$sexo"
        echo
    ;;

    remove)
        # Primeiro mostra a lista de usuários, depois pergunta
        echo "Lista dos usuários do sistema Z:"
        pega_campo 1 | sed 1d | tr \\n ' '
        echo
        echo
        echo -n "Qual usuário você quer remover? "
        read -r login
        echo

        # Vamos apagar ou puxar a orelha?
        if tem_chave "$login"; then
            apaga_registro "$login"
        else
            echo "Não, não, esse usuário nao esta aqui..."
        fi
        echo
    ;;

    *)
        # Qualquer outra opão é erro
        echo "Opção invalida $1"
        exit 1
esac