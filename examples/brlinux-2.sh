#!/usr/bin/env bash

# brlinux.sh
# Mostra as 5 últimas manchetes do BR-Linux
#
# Versão 2: Que procura texto
#
# Fabio Coelho, Abril de 2020

URL="http://br-linux.org"

# O padrão são linhas com "<h1><a href".
# O sed remove as tags HTML, restaura as aspas e
# apaga os espaços do inicio.
# Até a linha "Funçoes ZZ" é lixo.
# O head limita o numero de manchetes em 5.
# 
lynx -source "$URL" |
    grep '<h2><a href' |
    sed '
        s/<[^>]*>//g
        s/&quot;/"/g' |
        tr -d '\t' |
    head -n 5
