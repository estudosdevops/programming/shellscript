#!/usr/bin/env bash

# brlinux.sh
# Mostra as 5 últimas manchetes do BR-Linux
#
# Versão 3: Que procura no Feed XML
#
# Fabio Coelho, Abril de 2020

# O padrão são linhas com "<h1><a style".
# O sed remove as tags HTML, restaura as aspas e
# apaga ps espaços do inicio.
# Até a linha "Funçoes ZZ" é lixo.
# O head limita o numero de manchetes em 5.
#

feed() {
    lynx -source "$1" | grep '<title>' | tr -d \\t |
        sed 's/ *<[^>]*>//g; s/&quot;/"/g; 1d'
}
