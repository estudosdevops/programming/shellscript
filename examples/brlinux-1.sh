#!/usr/bin/env bash

# brlinux.sh
# Mostra as 5 últimas manchetes do BR-Linux
#
# Versão 1: Que procura texto
#

URL="http://br-linux.org"

# O padrão são as linhas que iniciam com maiúsculas.
# A primeira linha é apagada, pois é o nome do site.
#
lynx -dump -nolist "$URL" |
    grep '[A-Z]' |
    sed '1d' |
    head -n 5
