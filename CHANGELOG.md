# CHANGELOG

<!--- next entry here -->

## 0.9.0
2020-05-07

### Features

- **added:** examples with data bases textual (c62a24361d64b4c882f1a677e3da5646ae2730de)
- **added:** Funçoes de melhorias (09dc47d6d42b6d79bc5b99004599f0b3809f3b64)
- **added:** Jobs defalut, examples zuser.sh (161883a047466e4e51005c56412cf2f456f3d299)

## 0.8.0
2020-04-30

### Features

- **added:** Include eval call script parse.sh, clean up lines case (a15b552c4097d14acecaf38c977f15de9ae8afa1)

### Fixes

- **removed:** Line before_script (466c960112dec006121807bb56c64f21ed73c131)

## 0.7.0
2020-04-29

### Features

- **added:** Exemplos arquivos de configraçao (bf73b7ab9bab72b98252e6cfb59494a632afccaa)
- **added:** A new example of the parse  function (fbd969a52b1cd83c296ecf5ac2fba7d382668ae9)

### Fixes

- **added:** Melhoria no parse (03a531af13408dbeb28a871b09551ff3c5c87187)

## 0.6.0
2020-04-23

### Features

- **added:** Novos scripts exemplos (24b9c95a5d85960a968dbd956012a63076a15c87)

## 0.5.2
2020-04-16

### Fixes

- **added:** only tags in job test (3f3a0c209b3ef2a878a4da0d7fe2372cb38e9de7)

## 0.5.1
2020-04-16

### Fixes

- **added:** retry job test (9bd04cdd2db3a560f97505c642edc5be360569eb)

## 0.5.0
2020-04-16

### Features

- **added:** new scripts in examples (8f17d131bc65e8ccd0d867fd2f0f1a511ab215aa)

### Fixes

- **added:** retry job test, and allow_failure job debug (3d2a9c0e3c85dc5bd36b2e4cadb9d3fe7516895e)

## 0.4.0
2020-04-15

### Features

- **added:** Test de caracteres com bipe (9f990b9d51478586bff365656feba131c2ca410d)

## 0.3.0
2020-04-07

### Features

- **added:** gitignore (f15298ad36a1c0b9dd92758f26d44e88f57b86ea)
- **added:** job test debug (4811b75ed608c4bde1cf68fdb39c2caf4f0b4213)
- **added:** examplo debug.sh (6e11bb6f299a64707dceb431d918278cb950a268)

### Fixes

- **shellcheck:** SC2219 (0ba823763df77130e732478cbf4a197e60798ba4)
- **shellcheck:** SC2219 (b50f14a46a8296d0d6eb3d11b50ab3196166103f)

## 0.2.7
2020-04-06

### Fixes

- **changed:** Path scripts (ce882a64d37de9ea051a47bd098b8c2905646e10)

## 0.2.6
2020-04-06

### Fixes

- **changed:** Alterado nome pasta scripts para examples (6279efa355d3594305c56a3a4b7a854b47ce3cb0)
- **added:** file pre-commit.yaml (3787c00f38365b47265e577cf04f65984cf7c9f3)
- **added:** file --- (a5f1de325f9ae373e7aebc3b38ab03d789ff0959)
- **changed:** Name Files (2e18c95f6e1f0a2b8e0f78a922d4114124225b5a)

## 0.2.5
2020-03-27

### Fixes

- **added:** Testes ci-cd2 (bc791b66b935a7e05263fb9e88f2a35970b83f52)

## 0.2.4
2020-03-27

### Fixes

- **added:** Testes ci-cd (715032fb88601ad5b30a7c7e84961a1e412ac155)

## 0.2.3
2020-03-27

### Fixes

- **added:** Testes ci (e07e6cc3d48098bb0d0a9f1f26aef3ed6be1a799)

## 0.2.2
2020-03-27

### Fixes

- **changed:** Refatorando Dockerfile e gitlab-ci (11e2f057a4c722436b6fbf4382f7167ab8347037)
- **added:** Stage lint em gitlab-ci (e6a3e2946e3bea14590509232cf4a2bba31967b1)
- **added:** Signed-off-by: Fabio Coelho <fabiocruzcoelho@gmail.com> (b1c771fe81113acb5fe62bd991b2cef99a568803)
- **added:** Testes ci (75979d3699cbb9534e542b269189938e19fb749b)

## 0.2.1
2020-03-26

### Fixes

- **added:** Include gitlab-ci1 (b5a2c6bf11c6b6d08c681ffb88b3968850e16432)
- **image:** Version tag (01e2293ee39b9dd4514b4a5e1d34a1806e1b65ad)

## 0.2.0
2020-03-26

### Features

- **added:** Include image docker from registry gitlab (1166c728f067aaf4791f51db68f48369bb4071dc)
- **added:** Include gitlab-ci (d59a4edb4a428eaf3a0d723088063cc77bafdc5d)
- **added:** Include gitlab-ci (c0cc1926d262f7d8aa1f8933bf5c22adf92c8197)

### Fixes

- **remove:** Lines duplicated in gitlab-ci (8aeab5a895429db314d0e5f57f0bd9fb89d9f153)
- **added:** Include gitlab-ci (f53ff5aa2f8ff1ee45293f8276bdfd374458f9cd)
- **added:** Include gitlab-ci (ef323bcb69b893ba92e35f27aa34bc9e45fb72ff)

## 0.1.0
2020-03-26

### Features

- **added:** Include semantic-release (e3f793bafdf1e082a8cd17002987dd000e32e246)
- **added:** Include image docker from registry gitlab (1166c728f067aaf4791f51db68f48369bb4071dc)
- **added:** Include gitlab-ci (d59a4edb4a428eaf3a0d723088063cc77bafdc5d)
- **added:** Include gitlab-ci (c0cc1926d262f7d8aa1f8933bf5c22adf92c8197)

### Fixes

- **remove:** Lines duplicated in gitlab-ci (8aeab5a895429db314d0e5f57f0bd9fb89d9f153)
- **added:** Include gitlab-ci (f53ff5aa2f8ff1ee45293f8276bdfd374458f9cd)

## 0.1.0
2020-03-26

### Features

- **added:** Include semantic-release (e3f793bafdf1e082a8cd17002987dd000e32e246)
- **added:** Include image docker from registry gitlab (1166c728f067aaf4791f51db68f48369bb4071dc)
- **added:** Include gitlab-ci (d59a4edb4a428eaf3a0d723088063cc77bafdc5d)
- **added:** Include gitlab-ci (c0cc1926d262f7d8aa1f8933bf5c22adf92c8197)

### Fixes

- **remove:** Lines duplicated in gitlab-ci (8aeab5a895429db314d0e5f57f0bd9fb89d9f153)